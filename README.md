# sijishows项目本地部署文档

### 环境要求
不同系统环境找官方doc文档一步步来就行，都选LTS版本
 - Nodejs <LTS>
 - MariaDB <LTS>

**MariaDB安装完成后，一定记得导入测试数据！**

npm速度不理想的话，可以选择替换taobao源，否则请忽略下面npm的设置
```js
npm config set registry https://registry.npm.taobao.org --global
npm config set disturl https://npm.taobao.org/dist --global
```




## sijishow-backend 本地部署
 - 在项目根目录新建```models```文件夹
 - 将sijishows-model项目的所有```.js```文件考入```models```文件夹
 - 根据开发需求配置好```/config/config.js```(MariaDB配置，项目运行地址等等)
 - 根目录运行```npm install```安装项目依赖(若遇到安装错误,根据错误提示google一哈)
 - 根目录运行```npm start```启动项目





## sijishow-api 本地部署
 - 在项目根目录新建```models```文件夹
 - 将sijishows-model项目的所有```.js```文件考入```models```文件夹
 - 根据开发需求配置好```/config/config.js```(MariaDB配置，项目运行地址等等)
 - 根目录运行```npm install node-pre-gyp -g```(若遇到安装错误,根据错误提示google一哈)
 - 根目录运行```npm install canvas```(若遇到安装错误,根据错误提示google一哈)
 - 将根目录```package.json```中的```scripts```属性调整至如下
    ```json
    //其实就是添加npm start的启动方法
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "start": "node ./index",
        "start-pro": "NODE_ENV=pro node ./index"
    }
    ```
 - 根目录运行```npm start```启动项目





## sijishow-wechat-app 本地部署
 - 在项目根目录新建```models```文件夹
 - 将sijishows-model项目的所有```.js```文件考入```models```文件夹
 - 根据开发需求配置好```/config/config.js```(MariaDB配置，项目运行地址等等)
 - 进入```/client```文件夹运行```npm install```安装react项目依赖(若遇到安装错误,根据错误提示google一哈)
 - 在```/client```文件夹下面 ```npm run build```打包前端react项目
 - 回到根目录运行```npm install```安装依赖
 - 将根目录package.json中的scripts属性调整至如下
    ```json
    //其实就是添加npm start的启动方法
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "start": "node ./index",
        "start-pro": "NODE_ENV=pro PORT=3001 node ./index"
    }

    ```
 - 根目录运行```npm start```启动项目




# 特别注意
 - 项目由后端（backend）| 中间层（api）| 前端（wechat-app），三个独立服务组成。需要配置的地方挺多的，所以每个服务config配置的时候尽量写文档标注清晰。
 - ```wechat-app```中的```client```是一个纯前端项目，相关配置文件都在```/client/config```目录下（webpack的相关配置）,```/client/src```中文件修改都需要重新在```client```目录下重新运行```npm run build```编译一次。
 - 上面都是本地dev部署（注意各个项目config文件中dev环境和production环境的设置）